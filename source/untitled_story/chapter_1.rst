=========
Chapter 1
=========


"I don't know how people ever survived without g-drives," Kate sighed. Sometimes
trips like this could start to feel pretty long, even with a hyperspace engine
and someone to talk to.

"I guess it wouldn't be that big of a deal if you weren't used to having them,"
replied Theo. "But I'm sure people tended to stay home, anyway."

"Yeah," Kate said, distracted. She could feel her stomach rumbling, which made
it difficult to focus on anything else. "I'm gonna go find something to eat,"
she added, after a few moments of silence.

"Uh, okay. Joel has about an hour of sleep left, so try not to wake him up."

"Right," she replied, leaving the cockpit. Her claws scraped against the ship's
floors as she made her way to the kitchen. She didn't like trimming them. In
fact, there were a lot of things she didn't care for, such as being hungry, and
walking around a big, dark, lonely ship. She did, however, like Theo, so she
went with him on cargo trips like these anyway.

Eventually she found the kitchen and ate a sandwich she'd made earlier. It
contained several ingredients from her homeworld, Balta, which always helped
when she got into these weird moods.
