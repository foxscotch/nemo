=================
Table of Contents
=================

.. toctree::
   :maxdepth: 2
   
   untitled_story/index


Here's a cheatsheet_ for rST syntax, and the source_.

.. _cheatsheet: cheatsheet.html

.. _source: _sources/cheatsheet.txt


I really don't have a whole lot to talk about here, but I guess it's worth
mentioning that I have a special horizontal rule that is used with `|shr|`. You
can see what it looks like below this paragraph.

|shr|

It puts quite a bit of space between the two paragraphs, so I may change that.
But I do think, in context, that amount of space would be fitting. Just looks
kind of odd here. Also, "SHR" stands for "special horizontal rule," in case you
were wondering what level of creativity we're dealing with here.
